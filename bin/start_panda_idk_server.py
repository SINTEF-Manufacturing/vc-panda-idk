# coding=utf-8

"""
Script for launching a PIDKServer from the command-line.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2022"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import argparse
import logging

import panda_idk_server


logging.basicConfig()

ap = argparse.ArgumentParser()
ap.add_argument('--bind_host', type=str, default='127.0.0.1')
ap.add_argument('--port', type=int, default=2211)
args = ap.parse_args()

pidks = panda_idk_server.PIDKServer(bind_host=args.bind_host, port=args.port)
pidks.start()
