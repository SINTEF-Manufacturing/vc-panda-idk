# coding=utf-8

"""Test script for interacting with a PIDKClient on the
command-line. Does not run on Python 2.7 due to use of the argparse
package.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2022"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import argparse
import logging

import panda_idk_client


logging.basicConfig()

ap = argparse.ArgumentParser()
ap.add_argument('--server_host', type=str, default='127.0.0.1')
ap.add_argument('--port', type=int, default=2211)
args = ap.parse_args()

pidkc = panda_idk_client.PIDKClient(timeout=0.1,
                                    server_host=args.server_host,
                                    port=args.port)
