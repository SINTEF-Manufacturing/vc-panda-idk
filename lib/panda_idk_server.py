# coding=utf-8

"""
Module implementing the Panda IDK server class.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2022"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import struct
import socket
import threading
import time
import logging

import numpy as np
import math3d as m3d
import pymoco
# from network_utils.get_ip import get_own_ip


np.set_printoptions(precision=3)


class PIDKServer(threading.Thread):
    """Server class for answering requests to solving the inverse
    differential kinematics for the Panda robot.
    """
    
    # Request ID, current joint values, and target pose vector
    idk_req = struct.Struct('I7d6d')
    # Request ID and target joint values
    idk_resp_struct = struct.Struct('I7d')
    # Request ID and one byte of zero value to symbolize no solution
    nosol_resp_struct = struct.Struct('IB')

    def __init__(self,
                 excluded_joints: list[int] = [],
                 bind_host='127.0.0.1',
                 port=2211
                 ):
        super().__init__(daemon=True)
        self.bind_host = bind_host
        self.port = port
        self.fc = pymoco.kinematics.FrameComputer(
            rob_def=pymoco.robots.panda.Panda())
        self.idks = pymoco.kinematics.IDKSolver(
            frame_computer=self.fc,
            excluded_joints=excluded_joints)
        self.sock = socket.socket(family=socket.AF_INET,
                                  type=socket.SOCK_DGRAM)
        self.sock.bind((self.bind_host, self.port))
        self.pong_id = 0
        self._log = logging.getLogger(self.__class__.__name__)

    def run(self):
        while True:
            req, client = self.sock.recvfrom(1024)
            self._log.info(f'Request from {client}')
            if req == b'ping':
                self.sock.sendto(f'  pong_{self.pong_id}'.encode(), client)
                self.pong_id += 1
            elif len(req) == self.idk_req_struct.size:
                reqvals = self.idk_req_struct.unpack(req)
                req_id = reqvals[0]
                self._log.info(f'  req_id: {req_id}')
                q_start = np.array(reqvals[1:7+1])
                pv_tgt = m3d.PoseVector(reqvals[7+1:])
                t0 = time.time()
                q_tgt = self.idks.solve(q_start, pv_tgt)
                if q_tgt is None:
                    resp = self.nosol_resp_struct.pack(req_id, 0)
                else:
                    resp = self.idk_resp_struct.pack(*([req_id] + q_tgt.tolist()))
                self.sock.sendto(resp, client)
                self._log.info(f'  Solve time: {time.time()-t0:.3f}')
            else:
                self._log.info('Spurious')
