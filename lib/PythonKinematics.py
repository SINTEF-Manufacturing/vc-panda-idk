from vcPythonKinematics import *
import vcMatrix
import vcVector
from math import *

import panda_idk_client
reload(panda_idk_client)
print('!!! Reload !!!')

from panda_idk_client import PIDKClient
pidkc = PIDKClient(timeout=0.1)
 
PI = atan2(0,-1)
RAD_TO_DEG = 180.0/PI
DEG_TO_RAD = PI/180.0
ZERO_TOL = 0.0001

BACK = 4
DOWN = 2
FLOP = 1
# some of these can be exposed on the Param tab
SINGULARITY_TOLERANCE = 1.0*DEG_TO_RAD
POSITION_TOLERANCE = 1.0
 
#-------------------------------------------------------------------------------
global FirstCall, JS, JZO
#-------------------------------------------------------------------------------

FirstCall = True

# Define the amount of joints and their names
JOINT_COUNT = 7
JOINT_NAMES = ["A1","A2","A3","A4","A5","A6","A7"]
KINEMATIC_JOINT_COUNT = 7
# JS = [1]*JOINT_COUNT
# JZO = [0.0]*JOINT_COUNT

# Define the amount of configurations and their names
CONFIG_COUNT = 1 # 8
CONFIG_NAMES = ["0"]  #["010","110","000","100","011","111","001","101"]

# Define matrices for use in calculation. Get allocation out of the way.


def copyMatrix(target,source):
    target.N = source.N
    target.O = source.O
    target.A = source.A
    target.P = source.P


# Init kinematic object information
def OnInitKinObject( kinobj ):
    prop = kinobj.createProperty(VC_STRING, "Configuration", VC_PROPERTY_STEP)
    prop.Value = CONFIG_NAMES[0]
    prop.StepValues = CONFIG_NAMES


# Returns the amount of joints this kinematics handles
def OnGetJointCount():
    return JOINT_COUNT


def OnGetKinematicJointCount():
  return KINEMATIC_JOINT_COUNT


# Returns the indexed joint names
def OnGetJointName(index):
    return JOINT_NAMES[index]


def OnGetConfigurationCount():
    return CONFIG_COUNT


def OnGetCurrentConfiguration(kinobj):
    return CONFIG_NAMES.index(kinobj.Configuration)


def OnSetCurrentConfiguration(kinobj, cIndex):  
    kinobj.Configuration = CONFIG_NAMES[cIndex]
    
 
# Returns Kinematic chain target (matrix) value based on joint values
def OnForward(kinobj):
    if FirstCall:
        OnFinalize()
    #endif
 
    # nominal = GetNominalJoints(kinobj.JointValues)
    qs = kinobj.JointValues
    kinobj.Target = compute_forward(qs)
    return True


def compute_forward(qs):
    # print(nominal)
    m = vcMatrix.new()

    # A1
    m.rotateRelZ( qs[0] )
    m.translateRel( 0, 0, L01Z )
    
    # A2
    m.rotateRelY( qs[1] )
    
    # A3
    m.rotateRelZ( qs[2] )
    m.translateRel( 0, 0, L23Z )
    
    # A4
    m.translateRel( L34X, 0, 0 )
    m.rotateRelY( -qs[3] )  # N.B.: sign
    m.translateRel( 0, 0, L34Z )

    # A5
    m.translateRel( -L34X, 0, 0)
    m.rotateRelZ( qs[4] )
    m.translateRel( 0, 0, L45Z )
        
    # A6
    m.translateRel( 0, 0, L56Z )
    m.rotateRelY( -qs[5] )  # N.B.: sign

    
    # A7
    m.translateRel( L67X, 0, 0 )
    m.rotateRelZ( -qs[6] )  # N.B.: sign
    
    # Mount plate
    m.translateRel(0, 0, -L7MZ)
    m.rotateRelX(R7MX)
    
    return m
 

def matrix_to_p_rv(m):
  p = [0.001 * pc for pc in (m.P.X, m.P.Y, m.P.Z)]
  aa = m.getAxisAngle()
  rv = [aa.W * DEG_TO_RAD * aac for aac in (aa.X, aa.Y, aa.Z)]
  return (p, rv)


def OnInverse(kinobj):

    if FirstCall:
        OnFinalize()
    #endif

    # print('OnInverse:')
    q_start = [DEG_TO_RAD * q for q in kinobj.JointValues]
    # print '  q_start', q_start
    fwd = compute_forward(kinobj.JointValues)
    # print '  fwd:', matrix_to_p_rv(fwd)
    # print(kinobj.JointValues)
    
    p_tgt, rv_tgt = matrix_to_p_rv(kinobj.Target)
    # print '  p_tgt', p_tgt
    # print '  rv_tgt', rv_tgt
    q_tgt = pidkc.idk_request(q_start, p_tgt + rv_tgt)
    # print '  q_tgt', q_tgt
    if q_tgt is not None:
      q_tgt_deg = [q * RAD_TO_DEG for q in q_tgt]
      # kinobj.Solutions = [(VC_SOLUTION_REACHABLE, q_tgt_deg)] + 7 * [ (VC_SOLUTION_UNREACHABLE, 7*[0.0]) ]
      kinobj.Solutions = [(VC_SOLUTION_REACHABLE, q_tgt_deg)]
    else:
      # print '  No solution received'
      # kinobj.Solutions = 8 * [ (VC_SOLUTION_UNREACHABLE, 7*[0.0]) ]
      kinobj.Solutions = [ (VC_SOLUTION_UNREACHABLE, 7*[0.0]) ]
    # print()
    return True
    
    
# Called when component feature trees of the component are rebuilt,
# update parameters if needed
def OnRebuild():
    OnFinalize() 


# Called after component is loaded or copied
def OnFinalize():
    global comp, FirstCall
    global L01Z, L23Z, L34Z, L34X, L45Z, L56Z, L67X, L7MZ, R7MX

    FirstCall = False
    comp = getComponent()

    L01Z = comp.L01Z 
    L23Z = comp.L23Z 
    L34Z = comp.L34Z
    L34X = comp.L34X 
    L45Z = comp.L45Z 
    L56Z = comp.L56Z
    L67X = comp.L67X 
    L7MZ = comp.L7MZ
    R7MX = comp.R7MX

