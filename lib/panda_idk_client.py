# coding=utf-8

"""
Module implementing the Panda IDK client class.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2022"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import socket
import struct
import logging


class PIDKClient:
    """A client class for the panda inverse differential kinematics
    server.
    """

    # Request ID, current joint values, and target pose vector
    idk_req = struct.Struct('I7d6d')
    # Request ID and target joint values
    idk_resp_struct = struct.Struct('I7d')
    # Request ID and one byte of zero value to symbolize no solution
    nosol_resp_struct = struct.Struct('IB')

    def __init__(self, timeout=0.5, server_host='127.0.0.1', port=2211):
        self.timeout = timeout
        self.server_host = server_host
        self.port = port
        self.sock = socket.socket(family=socket.AF_INET,
                                  type=socket.SOCK_DGRAM)
        self.server_addr = (self.server_host, port)
        self.req_id = 0
        self._log = logging.getLogger(self.__class__.__name__)

    def ping(self):
        """Ping function to test connectivity to the server."""
        self._log.info('Pinging server ...')
        self.sock.sendto(b'ping', self.server_addr)
        try:
            self.sock.settimeout(self.timeout)
            resp, resp_addr = self.sock.recvfrom(1024)
        except socket.timeout:
            self._log.info('No pong from server')
        else:
            self._log.info(resp)

    def idk_request(self, q_start, pv_target):
        """Assume that q_start and pv_target are lists of 7 and 6 floats
        respectively.
        """
        req_list = [self.req_id] + q_start + pv_target
        req = self.idk_req_struct.pack(*req_list)
        try:
            self.sock.sendto(req, self.server_addr)
        except socket.gaierror:
            self._log.info('GetAddressError on sending to', self.server_addr)
        resp_req_id = -1
        resp_data = None
        attempt = 0
        while resp_req_id != self.req_id and attempt < 5:
            self._log.info(' Attempt %d' % attempt)
            try:
                self.sock.settimeout(self.timeout)
                resp, resp_addr = self.sock.recvfrom(1024)
            except socket.timeout:
                self._log.warn('No response from server.')
            else:
                if len(resp) == self.idk_resp_struct.size:
                    resp = self.idk_resp_struct.unpack(resp)
                    resp_req_id = resp[0]
                    resp_data = resp[1:]
                elif len(resp) == self.nosol_resp_struct.size:
                    resp = self.nosol_resp_struct.unpack(resp)
                    resp_req_id = resp[0]
                    self._log.warn('No solution for q_tgt found')
                if resp_req_id != self.req_id:
                    self._log.warn('Received request id (%d) differs from expected (%d)!' % (self.req_id, resp[0]))
            attempt += 1
        self.req_id += 1
        return resp_data
